# coding: utf-8

from django.apps import AppConfig


class NiamotoPlantnoteConfig(AppConfig):
    name = 'niamoto_plantnote'

    def ready(self):
        import niamoto_plantnote.signals

