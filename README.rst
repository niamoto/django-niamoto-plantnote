=====
django-niamoto-plantnote
=====

django-niamoto-plantnote is a reusable Django application providing integration
facilities for the niamoto system.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "niamoto_plantnote" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'niamoto_plantnote',
    ]

2. Run `python manage.py migrate` to create the niamoto_plantnote models.

